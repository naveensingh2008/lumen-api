<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // Disable timestamps
    public $timestamps = false;

    // Set a table name in database
    protected $table = 'products';
}
