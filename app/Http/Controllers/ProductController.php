<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Validator;
use Illuminate\Http\Request

class ProductController extends Controller
{
   

    public function productList()
    {
    	try
        {


        $productList = Product::where('status',1)->get();

        return $this->respondCreated([
            'message'   => 'Product listed succesfully.',
            'result'   => $productList
        ]);
        }
        catch(Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function categoryList()
    {
    	try
        {

        $categoryList = Category::where('status',1)->get();

        return $this->respondCreated([
            'message'   => 'Category listed succesfully.',
            'result'   => $categoryList
        ]);
        }
        catch(Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function productListByCategory(Request $request)
    {
    	try
        {

    	$validation = Validator::make($request->all(), [
            'category_id'          => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }


        $productList = Product::where('category_id',$request->category_id)->get();

        return $this->respondCreated([
            'message'   => 'Product listed succesfully.',
            'result'   => $productList
        ]);
        }
        catch(Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        }
    }




}