<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;

class AuthController extends Controller
{
    
    public function login(Request $request)
    {
        try
        {
        $validation = Validator::make($request->all(), [
            'email'   => 'required|max:255|email|unique:users,email',
            'password' => 'required',
            'device_type' => 'required',
            'device_token' => 'required'
        ]);

        if ($validation->fails()) {
            return $this->respondCreated([
                'message'   => $validation->messages()->first(),
            ]);
            //return $this->throwValidation($validation->messages()->first());
        }
        
            
            $credentials = $request->only(['email', 'password']);
            $user = $this->repository->findByEmail($request->email);
            
            if(!empty($user)){
                
                try {
                    
                    $passportToken = $user->createToken('API Access Token');
                    // Save generated token
                    $passportToken->token->save();
                    $token = $passportToken->accessToken;
                    
                    if ($user->confirmed == 0) {
                        return $this->respondCreated([
                                'message'   => 'Your email is not verify.Please verify your email.',
                                'is_confrimed'   => $user->confirmed,
                                'id'        => $user->id,
                                'token'     => $token,
                            ]);
                    }
                    
                    if (!Auth::attempt($credentials)) {
                        //return $this->throwValidation(trans('api.messages.login.failed'));
                        return $this->respondCreated([
                                'message'   => 'Invalid Credentials! Please try again.',
                            ]);
                    }
                    
                    $update = User::where('id',$user->id)->first();
                    
                    if(!empty($update)){
                        $update->device_type = $request->device_type;
                         $update->device_token = $request->device_token;
                         $update->save();
                    }
                    
                } catch (\Exception $e) {
                    return $this->respondInternalError($e->getMessage());
                }
                
            }else{
                return $this->respondCreated([
                                'message'   => 'Email/Mobile does not exist.',
                            ]);
            }

             return $this->respondCreated([
                    'message'   => 'Login successfully with your registered email.',
                    'token'     => $token,
                    'id'        => $user->id,
                    'is_confrimed'   => $user->confirmed,
                ]);
        }
        catch(Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        } 
       
    }
    

}
