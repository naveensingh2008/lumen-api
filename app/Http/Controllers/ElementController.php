<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Cart;
use Illuminate\Http\Request;

class ElementController extends Controller
{

     public function addToCart(Request $request)
    {
    	try
    	{
    	  $validation = Validator::make($request->all(), [
            'user_id'          => 'required',
            'product_id'          => 'required',
            'quantity'          => 'required',
            'amount'          => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }

        $cartDetail = Cart::where('user_id'$request->user_id)->where('product_id',$request->product_id)->first();
        if($cartDetail){
            $cartDetail->user_id = $request->user_id;
            $cartDetail->product_id = $request->product_id;
            $cartDetail->quantity = $request->quantity;
            $cartDetail->amount = $request->amount;
            $cartDetail->save();
        }else{

            $newcart = new Cart;
            $newcart->user_id = $request->user_id;
            $newcart->product_id = $request->product_id;
            $newcart->quantity = $request->quantity;
            $newcart->amount = $request->amount;
            $newcart->save();
        }

        return $this->respondCreated([
                    'message'   => 'Added to your cart!',
                ]);
        }
        catch(Exception $e)
		{
			return $this->respondInternalError($e->getMessage());
		} 
    }


    public function userCartList(Request $request)
    {
    	try
    	{

    	  $validation = Validator::make($request->all(), [
            'user_id'          => 'required',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }

        $cartList = Cart::with('product','user','product.category')->where('user_id'$request->user_id)->get();

        return $this->respondCreated([
                    'message'   => 'Cart List',
                    'result' => $cartList
                ]);
    	}
        catch(Exception $e)
		{
			return $this->respondInternalError($e->getMessage());
		} 
        
    }


}