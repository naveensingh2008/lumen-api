<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('/login', 'AuthController@login');
    $router->get('/product_list', 'ProductController@productLsit');
    $router->get('/category_list', 'ProductController@categoryList');
    $router->post('/product_list_by_category', 'ProductController@productListByCategory');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/add_to_cart', 'ElementController@addToCart');
         $router->post('/user_cart_list', 'ElementController@userCartList');
    });
});

